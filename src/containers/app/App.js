import React from "react";
import { ApolloProvider } from "react-apollo";

import Router from "./router";
import { client } from "./client";

const App = () => (
  <ApolloProvider client={client}>
    <Router />
  </ApolloProvider>
);

export default App;
