import Home from "../home";

const routes = [
  {
    id: "home",
    name: "Home",
    component: Home,
    path: "/",
    routes: []
  }
];

export default routes;
