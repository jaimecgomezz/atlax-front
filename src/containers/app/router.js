import React from "react";
import { BrowserRouter, Route } from "react-router-dom";

import routes from "./routes";

const AppRouter = () => (
  <BrowserRouter>
    {routes.map(({ path, component, ...rest }) => (
      <Route exact path={path} component={component} {...rest} />
    ))}
  </BrowserRouter>
);

export default AppRouter;
