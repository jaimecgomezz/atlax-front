import React from "react";
import { Layout, PageHeader } from "antd";

const HeaderComponent = () => (
  <Layout>
    <PageHeader />
  </Layout>
);

export default HeaderComponent;
